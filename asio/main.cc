#include <iostream>
#include <utility> // std::pair
#include <stdexcept>

#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <cstdlib>

#define TIMEOUT 5

class ReadStdinTimeout
{
	int timeout;
	boost::asio::io_service io;

	boost::asio::posix::stream_descriptor input;
	boost::asio::deadline_timer timer;
public:
	ReadStdinTimeout(int t = TIMEOUT):
		timeout{t},
		io{},
		input{io, ::dup(STDIN_FILENO)},
		timer{io}
	{}

	// returns pair [str, is_timeout]
	// - str contains non-line from stdin
	// - str is empty if EOF
	// - is_timeout is true when timeout occurs
	std::pair<std::string, bool> readline() {
		bool timer_result = false;
		bool read_result = false;

		timer.expires_from_now( boost::posix_time::seconds(timeout) );
		timer.async_wait( [&timer_result](const boost::system::error_code& e) {
			std::cerr << "timer result\n";
			timer_result = (e != boost::asio::error::operation_aborted);
		});
		
		boost::asio::streambuf buffer;
		boost::asio::async_read_until(
			input,
			buffer,
			'\n',
			[&read_result](boost::system::error_code b, const long unsigned int& /*l*/) {
				read_result = true;
			}
		);
		io.reset();

		while ( io.run_one() )
		{
			if ( read_result )
			{
				timer.cancel();
				auto size = buffer.size();
				buffer.commit(size);
				std::string line{
					boost::asio::buffers_begin(buffer.data()),
					boost::asio::buffers_end(buffer.data()),
				};
				return {line, false};
			}
			else if ( timer_result )
			{
				return {"", true};
			}
		}
		throw std::runtime_error("Unreachable");
	}
};

int main() {
	ReadStdinTimeout r(5);
	while (true) {
		auto [line, timeout] = r.readline();
		if (timeout) {
			std::cerr << "Timeout reading stdin\n";
			return 1;
		}
		if (line == "") {
			// EOF
			break;
		}
		std::cerr << "Read: " << line;
	}
}
